#!/bin/bash

# Make sure you have build the docker image
# Take a look at https://gitlab.com/doktormike/valgrind-for-everyone
# ulimit is needed for valgrind compatibility. Feel free to bump it if it's not
# enough.
docker run -it -v $PWD:/tmp -w /tmp valgrind4all:1.0 /bin/sh -c "g++ $1; ulimit -n 10000; valgrind ./a.out"
