dbuild:
	docker build -t "valgrind4all:1.0" . 

clean:
	cd src; rm -rf *.gcda *.gcno a.out

coverage: clean
	cd src; g++ -O0 -coverage leaky.cpp && ./a.out && gcov leaky.cpp

