# Valgrind on Multiple CPU Architectures with Docker

[![pipeline status](https://gitlab.com/doktormike/valgrind-for-everyone/badges/main/pipeline.svg)](https://gitlab.com/doktormike/valgrind-for-everyone/-/commits/main)
[![coverage report](https://gitlab.com/doktormike/valgrind-for-everyone/badges/main/coverage.svg)](https://gitlab.com/doktormike/valgrind-for-everyone/-/commits/main)
[![Latest Release](https://gitlab.com/doktormike/valgrind-for-everyone/-/badges/release.svg)](https://gitlab.com/doktormike/valgrind-for-everyone/-/releases)


## Table of Contents
- [Introduction](#introduction)
- [Why Docker for CPU Architecture Agnosticism](#why-docker-for-cpu-architecture-agnosticism)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Usage](#usage)
- [Supported Architectures](#supported-architectures)
- [Contributing](#contributing)
- [License](#license)

## Introduction

This repository provides a solution for running [Valgrind](http://valgrind.org/) on various CPU architectures using Docker. Valgrind is a powerful tool for memory analysis, debugging, and profiling. However, its compatibility with different CPU architectures can be challenging. Docker helps make Valgrind more CPU architecture agnostic by containerizing the environment. The idea for this comes from [Ross](https://medium.com/@massey0ross/valgrind-in-macos-with-docker-3b0e4bbdece1).

## Why Docker for CPU Architecture Agnosticism

Valgrind relies on low-level CPU features for its functionality, which can vary significantly between different CPU architectures. Docker containers encapsulate the entire runtime environment, ensuring that Valgrind runs consistently across various architectures without worrying about system-specific dependencies. This makes it easier to analyze and debug code on multiple CPU architectures, promoting portability and cross-platform development.

## Getting Started

### Prerequisites

Before you begin, make sure you have the following prerequisites installed:

- [Docker](https://www.docker.com/)

### Usage

1. Clone this repository to your local machine:

   ```shell
   git clone https://gitlab.com/doktormike/valgrind-for-everyone.git
   ```
2. Navigate to the new folder:
   ```shell
   cd valgrind-for-everyone
   ```
3. Build the docker image:
   ```shell
   make dbuild
   ```
4. Test the little bugger:
   ```shell
   cd src 
   ../valgrind4all.sh leaky.cpp
   ```

The last step should give you the following output:

```shell
==1== Memcheck, a memory error detector
==1== Copyright (C) 2002-2022, and GNU GPL'd, by Julian Seward et al.
==1== Using Valgrind-3.21.0 and LibVEX; rerun with -h for copyright info
==1== Command: ./a.out
==1== 
==1== 
==1== HEAP SUMMARY:
==1==     in use at exit: 72,708 bytes in 2 blocks
==1==   total heap usage: 3 allocs, 1 frees, 72,712 bytes allocated
==1== 
==1== LEAK SUMMARY:
==1==    definitely lost: 4 bytes in 1 blocks
==1==    indirectly lost: 0 bytes in 0 blocks
==1==      possibly lost: 0 bytes in 0 blocks
==1==    still reachable: 72,704 bytes in 1 blocks
==1==         suppressed: 0 bytes in 0 blocks
==1== Rerun with --leak-check=full to see details of leaked memory
==1== 
==1== For lists of detected and suppressed errors, rerun with: -s
==1== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
```

If you want to you can copy `valgrind4all.sh` script to your `bin` directory or 
any other directory that is in your executable path. This way you can call it 
wherever you are developing.

## Supported Architectures

This repository currently supports the following CPU architectures:

- x86_64
- ARMv7
- ARMv8
- M1
- M2

If you need support for additional architectures, feel free to contribute or open an issue.

## Contributing

Contributions to this repository are welcome! If you encounter any issues or have improvements to suggest, please feel free to open an issue or submit a pull request.

## License

This project is licensed under the MIT License.
