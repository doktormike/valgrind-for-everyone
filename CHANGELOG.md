# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0 (2023-08-28)


### Features

* fixed up valgrind for all with samples. ([a421d13](https://gitlab.com/doktormike/valgrind-for-everyone/commit/a421d130e2ef1e0c178e080450d73d7cc1b78cd9))
* initial import of docker valgrind ([545dd4b](https://gitlab.com/doktormike/valgrind-for-everyone/commit/545dd4b539614426c7859a51ea34aad1ccae25b6))
* more leaky stuff with proper handling. ([f770c67](https://gitlab.com/doktormike/valgrind-for-everyone/commit/f770c67d50fcb9a12bd149381eee166a6199b763))


### Documentation

* added readme. ([0302aeb](https://gitlab.com/doktormike/valgrind-for-everyone/commit/0302aebdecfb40638c1c04d2944ba384be8c7270))
* badges are a must! ([739fdb6](https://gitlab.com/doktormike/valgrind-for-everyone/commit/739fdb696828e61300500713ea3a47fb7ea45ae8))
* source ([6688b5f](https://gitlab.com/doktormike/valgrind-for-everyone/commit/6688b5fa11caf2218a7939180e80483300dbf4b9))
