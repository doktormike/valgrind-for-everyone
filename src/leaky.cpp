// A sample of a stupid memory leak :)
// Valgrind should report this
int main() {
    // OK
    int * p = new int;
    delete p; 

    // Memory leak incoming
    int * q = new int;
}
